{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      flake-utils,
      ...
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        packages = rec {
          stackfile =
            with pkgs;
            rustPlatform.buildRustPackage {
              name = "stackfile";
              src = lib.cleanSource ./.;
              cargoHash = "sha256-ewpTaiBXC02ODR/pgUFLglY81opDQCPeZnlj2OiWl00=";
            };
          stackfileCC =
            with pkgs;
            stdenv.mkDerivation {
              name = "stackfile";
              src = ./src/main.cc;
              dontUnpack = true;
              buildPhase = ''
                g++ $src -o stackfile
              '';
              installPhase = ''
                mkdir -p $out/bin
                cp stackfile $out/bin
              '';
            };
          default = stackfile;
        };
      }
    );
}
