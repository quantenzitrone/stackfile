# Stackfile

This small program uses a plain file as persistent memory for a stack (LIFO (Last In First Out)) of Strings.

Currently the classic `push` `pop` `empty` operations are supported.

Additional to the main rust implementation there exists a horribly unsafe C++ implementation, which will not be worked on anymore. Still doesn't hurt to keep it around.

## Plans

- implement some interfaces to make it interoperable with other rust code
- improve the code

## Command Line Usage

```sh
USAGE:
    stackfile <filename> <command> [<args>]

COMMANDS:
    stackfile <filename> empty
        -> exits with exit code 0 if the file is empty or nonexistent and 1 otherwise

    stackfile <filename> empty2
        -> prints 'true' if the file is empty or nonexistent and 'false' otherwise

    stackfile <filename> pop
        -> pops a line from the filestack and prints it

    stackfile <filename> push <text>
        -> pushes <text> to the stack (appends it to the file)

EXAMPLE:
    $ stackfile ./example.txt push "hello world"
    $ stackfile ./example.txt empty2
    false
    $ stackfile ./example.txt pop
    hello world
```