/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::{
    fs::{File, OpenOptions},
    io::SeekFrom,
    os::unix::fs::FileExt,
    path::Path,
    process::exit,
};

use rev_buf_reader::RevBufReader;
use std::io::{BufRead, Seek};

// "class" thats uses a utf8 text file as memory for saving a stack of strings
struct FileStack {
    file: File,
}

impl FileStack {
    // constructor
    pub fn new(filename: &Path) -> Result<Self, String> {
        // test if the the file exists, if not create it
        if !filename.exists() {
            // create the parent directory if it doesn't exist
            let parent = filename.parent().unwrap_or(&Path::new("."));
            if !parent.is_dir() {
                match std::fs::create_dir_all(parent) {
                    Ok(()) => {}
                    Err(e) => return Err(e.to_string()),
                };
            }
            match File::create(filename) {
                Ok(_) => {}
                Err(e) => return Err(e.to_string()),
            };
        }
        // check if the existing path is a file
        else if !filename.is_file() {
            return Err(String::from("Given Path exists and is not a regular File"));
        };

        // try to open the file with the required permissions
        match OpenOptions::new().read(true).write(true).open(filename) {
            Ok(file) => Ok(FileStack { file }),
            Err(e) => Err(e.to_string()),
        }
    }

    // pushes a string to the stack
    // aka appends a line to the file
    pub fn push(&mut self, line: String) {
        if !self.empty() {
            let len = self.bytes();
            let _ = self.file.write_all_at(b"\n", len);
        };
        let len = self.bytes();
        let _ = self.file.write_all_at(line.as_bytes(), len);
    }

    // pops a string from the stack
    // aka removes the last line from the file and returns it
    pub fn pop(&mut self) -> Result<String, String> {
        if self.empty() {
            Err(String::from("Cannot pop from empty file."))
        } else {
            // get the line
            let mut reader = RevBufReader::new(&self.file);
            let mut line = String::new();
            // get the new file size
            let linelen = match reader.read_line(&mut line) {
                Ok(r) => r,
                Err(e) => return Err(e.to_string()),
            };
            let len = self.bytes();
            let newsize = if len == linelen as u64 {
                0
            } else {
                len - linelen as u64 - 1
            };
            // truncate the file (remove the last line)
            match self.file.set_len(newsize) {
                Err(e) => return Err(e.to_string()),
                Ok(_) => {}
            };
            // return the line
            Ok(line)
        }
    }

    pub fn bytes(&mut self) -> u64 {
        self.file.seek(SeekFrom::End(0)).unwrap_or(0)
    }
    pub fn empty(&mut self) -> bool {
        self.bytes() == 0
    }
}

fn help() {
    print!(
        "USAGE:
    stackfile <filename> <command> [<args>]

COMMANDS:
    stackfile <filename> empty
        -> exits with exit code 0 if the file is empty or nonexistent and 1 otherwise

    stackfile <filename> empty2
        -> prints 'true' if the file is empty or nonexistent and 'false' otherwise

    stackfile <filename> pop
        -> pops a line from the filestack and prints it

    stackfile <filename> push <text>
        -> pushes <text> to the stack (appends it to the file)

EXAMPLE:
    $ stackfile ./example.txt push \"hello world\"
    $ stackfile ./example.txt empty2
    false
    $ stackfile ./example.txt pop
    hello world
"
    );
}

fn main() {
    // get args
    let mut args = std::env::args();
    // skip arg 0
    args.next();

    // filename arg 1
    let mut my_stack = match args.next() {
        Some(s) => match FileStack::new(Path::new(&s)) {
            Ok(r) => r,
            Err(e) => {
                eprintln!("Error creating FileStack: {}", e);
                exit(1);
            }
        },
        None => {
            help();
            exit(1);
        }
    };
    // eprintln!("using file {}", &my_stack.filename);

    // command arg 2
    let command = match args.next() {
        Some(s) => s,
        None => {
            help();
            exit(1);
        }
    };

    if command == "empty" {
        if my_stack.empty() {
            exit(0);
        } else {
            exit(1);
        }
    } else if command == "empty2" {
        if my_stack.empty() {
            println!("true");
        } else {
            println!("false");
        }
    } else if command == "pop" {
        let result = my_stack.pop();
        match result {
            Ok(line) => {
                println!("{}", line);
            }
            Err(e) => {
                eprintln!("Error from pop(): {}", e);
                exit(1);
            }
        }
    } else if command == "push" {
        let line = match args.next() {
            Some(s) => s,
            None => {
                help();
                exit(1);
            }
        };
        my_stack.push(line);
    } else {
        help();
        exit(1);
    };
}
