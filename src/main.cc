/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;
class fileStack {
	string filename;

  public:
	bool empty() {
		ifstream infile(filename);
		bool result = (infile.peek() == ifstream::traits_type::eof());
		infile.close();
		return result;
	};

	string pop() {
		assert(!empty());
		fstream file(filename);
		string lastline;
		int newlength;
		// get the last line and new length
		while (file.peek() != ifstream::traits_type::eof()) {
			newlength = file.tellg();
			getline(file, lastline);
		}
		file.close();
		// also remove the \n
		if (newlength > 0)
			--newlength;
		truncate(filename.c_str(), newlength);
		return lastline;
	};

	void push(const string &line) {
		ofstream outfile(filename, fstream::app);
		if (!empty())
			outfile << "\n";
		outfile << line;
		outfile.close();
	}

	fileStack(string filename) {
		this->filename = filename;
		// because this is tested here,
		// assume file is openable for all later operations
		fstream file(filename);
		if (!file.is_open())
			throw runtime_error("could not open file " + filename);
		file.close();
	}
};

int main(int argc, char **argv) {
	if (argc < 3) {
		cout << "Usage:\n"
		     << "  " << argv[0] << " <relative/path/to/file> push <string>\n"
		     << "     -> adds the string at the end of the file\n"
		     << "  " << argv[0] << " </absolute/path/to/file> pop\n"
		     << "     -> removes the last line from the file and prints it\n"
		     << "        if the file is empty it exits with 1\n"
		     << "  " << argv[0] << "\n"
		     << "     -> prints this help\n";
		return 0;
	}
	fileStack mystack(argv[1]);
	if (string(argv[2]).compare("push") == 0) {
		assert(argc >= 4);
		mystack.push(argv[3]);
	} else if (string(argv[2]).compare("pop") == 0) {
		if (mystack.empty()) {
			return 1;
		} else {
			cout << mystack.pop();
		}
	} else if (string(argv[2]).compare("empty") == 0) {
		if (!mystack.empty())
			return 1;
	}
	return 0;
}